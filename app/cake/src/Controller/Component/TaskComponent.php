<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Task component
 */
class TaskComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    protected $redisinstance;

    public function initialize($config)
    {
        parent::initialize($config);
        try {
            $this->redisinstance = new \Redis();
            $this->redisinstance->connect('redis');
            $this->redisinstance->select(9);
        } catch (\RedisException $e) {
            $this->log('Cannot create a redis session', 'error');
        };
    }

    public function enqueue($jobobject)
    {
        if (!$this->redisinstance->exists("cdli:job:{$jobobject->id}")) {
            return false;
        }
        $this->redisinstance->lpush('cdli:queue:' . $jobobject->queue, $jobobject->id);
        return true;
    }

    public function result($jobobject)
    {
        if (!$this->redisinstance->exists("cdli:result:{$jobobject->id}")) {
            return false;
        }
        $val = $this->redisinstance->get("cdli:result:{$jobobject->id}");
        $this->log($val, "debug");
        return $val;
    }

    
    public function create($queue, $function, $payload, $enqueue = true, $objects = null, $ttl = '600')
    {
        $id = uniqid();
        $this->register_queue($queue);
        $meta = array('status' => 'stacked', 'function' => $function ,'data' => json_encode($payload), 'time' => time(), 'ttl' => $ttl, 'session' => session_id());
        if ($objects) {
            $meta = array_merge($meta, ['storage' => json_encode($objects)]);
        }
        $this->redisinstance->hmset('cdli:job:' . $id, $meta);
        
        $jobobj = (object)['id'=> $id,'queue' => $queue, 'function' => $function];
        if ($enqueue) {
            $this->enqueue($jobobj);
        }
        return $jobobj;
    }
    
    private function register_queue($queuename)
    {
        return $this->redisinstance->sadd('cdli:queues', $queuename) ? true : false ;
    }
}
