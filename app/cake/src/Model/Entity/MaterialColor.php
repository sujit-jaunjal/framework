<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MaterialColor Entity
 *
 * @property int $id
 * @property string|null $material_color
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class MaterialColor extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material_color' => true,
        'artifacts' => true
    ];

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'cdli:type_color',
            'crm:P1_is_identified_by' => [
                '@type' => 'crm:E41_Appellation',
                'rdfs:label' => $this->material_color
            ],
            'crm:P56i_is_found_on' => self::getEntities($this->artifacts)
        ];
    }
}
